﻿namespace ProyectBD
{
	partial class Form1
	{
		/// <summary>
		/// Variable del diseñador necesaria.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Limpiar los recursos que se estén usando.
		/// </summary>
		/// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Código generado por el Diseñador de Windows Forms

		/// <summary>
		/// Método necesario para admitir el Diseñador. No se puede modificar
		/// el contenido de este método con el editor de código.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.Login = new System.Windows.Forms.Label();
			this.TxUsuario = new System.Windows.Forms.TextBox();
			this.TxContraseña = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// Login
			// 
			this.Login.AutoSize = true;
			this.Login.Font = new System.Drawing.Font("Impact", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Login.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.Login.Location = new System.Drawing.Point(122, 47);
			this.Login.Name = "Login";
			this.Login.Size = new System.Drawing.Size(65, 29);
			this.Login.TabIndex = 0;
			this.Login.Text = "Login";
			// 
			// TxUsuario
			// 
			this.TxUsuario.BackColor = System.Drawing.Color.Gray;
			this.TxUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.TxUsuario.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.TxUsuario.ForeColor = System.Drawing.SystemColors.ControlLightLight;
			this.TxUsuario.Location = new System.Drawing.Point(28, 150);
			this.TxUsuario.Name = "TxUsuario";
			this.TxUsuario.Size = new System.Drawing.Size(255, 20);
			this.TxUsuario.TabIndex = 4;
			this.TxUsuario.Text = "Usuario";
			this.TxUsuario.TextChanged += new System.EventHandler(this.TxUsuario_TextChanged);
			// 
			// TxContraseña
			// 
			this.TxContraseña.BackColor = System.Drawing.Color.Gray;
			this.TxContraseña.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.TxContraseña.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.TxContraseña.ForeColor = System.Drawing.SystemColors.ControlLightLight;
			this.TxContraseña.Location = new System.Drawing.Point(28, 210);
			this.TxContraseña.Name = "TxContraseña";
			this.TxContraseña.Size = new System.Drawing.Size(255, 20);
			this.TxContraseña.TabIndex = 5;
			this.TxContraseña.Text = "Contraseña";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Black;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(315, 334);
			this.Controls.Add(this.TxContraseña);
			this.Controls.Add(this.TxUsuario);
			this.Controls.Add(this.Login);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.MinimizeBox = false;
			this.Name = "Form1";
			this.Opacity = 0.9D;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Login";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.Label Login;
		private System.Windows.Forms.TextBox TxUsuario;
		private System.Windows.Forms.TextBox TxContraseña;
	}
}

